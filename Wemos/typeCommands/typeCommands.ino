#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
//function defs
void connectToNetwork();
bool parsePacket();
//constants
#define VERBOSE false//how much debug to show
#define PACKET_SIZE 1024
const char* ssid     = "MildendoNet";
const char* password = "d0g.d1nr";
const unsigned int localPort = 2390;
const char* broadcastAddr = "192.168.1.255";
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
char packetBuffer[PACKET_SIZE]; //buffer to hold incoming packet

WiFiUDP udp;
void setup() {
  Serial.begin(115200);//for io
  connectToNetwork();
  //now begin a UDP server
  udp.begin(localPort);
  Serial.println("Type Commands");  
  pinMode(D5, OUTPUT);
  digitalWrite(D5, HIGH);
}

void loop() {
  if (stringComplete) {
    Serial.println(inputString);//echo command
    udp.beginPacket(broadcastAddr, localPort);
    udp.write(inputString.c_str());
    udp.endPacket();
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  //serial event appers not to work on wemos d1
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '.' || inChar == '\n') {
      stringComplete = true;
    }
    else
    {
      // add it to the inputString:
      inputString += inChar;
    }

  }
  parsePacket();
}

/*
  SerialEvent occurs whenever a new data comes in the
  hardware serial RX.  This routine is run between each
  time loop() runs, so using delay inside loop can delay
  response.  Multiple bytes of data may be available.
*/

void connectToNetwork()
{
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);//actual connect
  while (WiFi.status() != WL_CONNECTED)//spin waiting
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

bool parsePacket()
{
  // if there's data available, read a packet
  int packetSize = udp.parsePacket();
  if (packetSize)
  {
    if (VERBOSE)Serial.print("Received packet of size ");
    if (VERBOSE)Serial.println(packetSize);
    if (VERBOSE)Serial.print("From ");
    if (VERBOSE)
    { IPAddress remoteIp = udp.remoteIP();
      Serial.print(remoteIp);
    }
    if (VERBOSE)Serial.print(", port ");
    if (VERBOSE)Serial.println(udp.remotePort());

    // read the packet into packetBufffer
    int len = udp.read(packetBuffer, PACKET_SIZE);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    Serial.println("Contents:");
    Serial.println(packetBuffer);

    return true;
  }
}
