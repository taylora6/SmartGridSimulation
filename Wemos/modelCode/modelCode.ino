/*
   sets up wifi on a D1 and alocate each a name in the order
   that they are connected.
   REMEMBER: LONG LEG OF LED IS + SHORT IS -
   I TAKE NO RESPONSIBILITY IF YOU BREAK THINGS
   currently done: ids, led opperation on digital pins,groups
   not done: sensor on analogue,
  notes:
  D4 onboard led
   Sample commands:
   General command
  <ID [1..1]><GROUP [0..1]><COMMAND [1..1]><GPIO [1..*]>
  request to join
  <-1><-1><join><invalid_ID>
  ack join
  <invalid_ID><-1><allocate><new_ID>
  turn off arduino 1's led on d4
  <1><><off><led_D4>
  turn on arduino 3's led on d6
  <3><><on><led_D6>
  set arduino 2's leds on D4&D7 flashing
  <2><><flash><led_D4,led_D7>
  put arduino 1,2,3 into group 5 (addressed to all but can be filtered)
  <-1><5><group><1,2,3>
  Turn all of group 5's leds on D4 on
  <><5><on><led_D4>
  See system
  <-1><><ping><>
  <-1><><pingserver><>
  allow nameserver to allocate id
  tell server i exsist
  <><><server><random int>
  override
  <random int><><override><new id>
*/

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>


//constants
const char* ssid     = "MildendoNet";
const char* password = "d0g.d1nr";
char  broadcastIP[] = "192.168.1.255";
const unsigned int localPort = 2390;
#define LED_PIN D5//use for debug
#define PACKET_SIZE 1024
#define VERBOSE false//how much debug to show
#define REMOTE_DEBUG false//how much debug to show
#define ALONE_TIMEOUT 5000//how long to wait to see if i'm the only node
#define FLASH_PERIOD 1000
//function defs
void connectToNetwork();
void setAndTestOutputHardware();
bool parsePacket();
void requestID();
void executeCommand(char* command, char* params, char* groupToAdd);
void setPinOn(int pin);
void setPinOff(int pin);
void setPinFlash(int pin);
int pinNameToNumber(char* pin);
void updateFlash();
//global variables
char packetBuffer[PACKET_SIZE]; //buffer to hold incoming packet
char  ReplyBuffer[] = "acknowledged";
WiFiUDP udp;
int ID = NULL;
int group = NULL;
bool realIDAllocated = false;
bool lastAllocatedID = false;
unsigned long requestedID;
int flashing[] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}; //wastful way of pin state using pin numbers as index
unsigned long lastFlashTime;
bool nameServerAvailable = true;
int nameServerID;//random in outside normal range of names
IPAddress myIp;//to init rand generator

void setup() {
  Serial.begin(115200);//for io
  connectToNetwork();
    nameServerID=rand()+200;
  //set any output we are using and test it
  setAndTestOutputHardware();
  //now begin a UDP server
  udp.begin(localPort);
  if (nameServerAvailable)
  { //check to see if we can get name from server
    udp.beginPacket(broadcastIP, localPort);
    char sprintBuffer [50];
    sprintf(sprintBuffer, "%s%i%s", "<><><server><", nameServerID, ">");
    Serial.println(sprintBuffer);
    udp.write(sprintBuffer);
    udp.endPacket();
  }
    lastFlashTime = millis();
  nameServerAvailable = false; //unless we hear back
  while (lastFlashTime + ALONE_TIMEOUT < millis())
  { //wait for server to respond
    parsePacket();
  }
}
void loop()
{
  if (realIDAllocated == false)
  {
    if (ID == NULL)
    {
      requestID();
    }
    if (requestedID + ALONE_TIMEOUT < millis())
    { //ive waited long enough for a name
      realIDAllocated = true;
      ID = 1;
      lastAllocatedID = true;
      if (REMOTE_DEBUG)
      {
        udp.beginPacket(broadcastIP, localPort);
        char sprintBuffer [50];
        sprintf(sprintBuffer, "%s", "<><><ignore><allocated my ID as 1>");
        if (VERBOSE)Serial.println(sprintBuffer);
        udp.write(sprintBuffer);
        udp.endPacket();
      }
      Serial.println("Gave up waiting for an ID, allocated 1");
    }
  }
  parsePacket();
  updateFlash();
}

void connectToNetwork()
{
  if (VERBOSE)Serial.print("Connecting to ");
  if (VERBOSE)Serial.println(ssid);
  WiFi.begin(ssid, password);//actual connect
  while (WiFi.status() != WL_CONNECTED)//spin waiting
  {
    delay(500);
    if (VERBOSE)Serial.print(".");
  }
  if (VERBOSE)Serial.println("");
  Serial.println("WiFi connected");
  if (VERBOSE)Serial.println("IP address: ");
  myIp=WiFi.localIP();
  if (VERBOSE)Serial.println(myIp);
  //ust use full ip as seed as only last byte doesn't work 
  //seeding with a single byte behaves differntly to same int
  int seed=(int)myIp[0];
  seed=seed<<8;
  seed+=(int)myIp[1];
  seed=seed<<8;
  seed+=(int)myIp[2];
  seed=seed<<8;
  seed+=(int)myIp[3];
 randomSeed(seed);
}
void setAndTestOutputHardware()
{
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  delay(500);
  digitalWrite(LED_PIN, HIGH);
  delay(1000);
  digitalWrite(LED_PIN, LOW);
}
bool parsePacket()
{
  // if there's data available, read a packet
  int packetSize = udp.parsePacket();
  if (packetSize)
  {
    if (VERBOSE)Serial.print("Received packet of size ");
    if (VERBOSE)Serial.println(packetSize);
    if (VERBOSE)Serial.print("From ");
    if (VERBOSE)
    { IPAddress remoteIp = udp.remoteIP();
      Serial.print(remoteIp);
    }
    if (VERBOSE)Serial.print(", port ");
    if (VERBOSE)Serial.println(udp.remotePort());

    // read the packet into packetBufffer
    int len = udp.read(packetBuffer, PACKET_SIZE);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    if (VERBOSE)Serial.println("Contents:");
    Serial.println(packetBuffer);
    //break up the packet to parts
    char *partPointer;//for the end of part when using ne3sted strtok
    char* part = strtok_r(packetBuffer, ">", &partPointer);
    //using strtok_r as nesting strtok fails due to a single end pointer
    bool forMe = false;
    char* command;
    char* params;
    char* groupToAdd;
    int currentPart = 0;
    while (part != NULL)
    {
      currentPart++;
      part++;
      //examine the part
      if (currentPart == 1 )
      {
        if (strlen(part) > 0 )
        { //is id
          char *subpartPointer;
          char* subpart = strtok_r(part, ",>", &subpartPointer);
          while (subpart != NULL)
          { //read all the , seperated ids
            if (atoi(subpart) == -1 || atoi(subpart) == ID)
            { //if the name is all or me
              forMe = true;
            }
            if (atoi(subpart) == nameServerID)
            { //if is for my name server alias
              nameServerAvailable = true;
              forMe = true;
            }
            //get next part
            subpart = strtok_r (NULL,  ",>", &subpartPointer);
          }
        }
      }
      else if (currentPart == 2)
      {
        groupToAdd = part; //save the group incase it is a group command
        if (strlen(part) > 0 )
        {
          char *subpartPointer;
          char* subpart = strtok_r(part, ",>", &subpartPointer);
          while (subpart != NULL)
          { //read all the , seperated ids
            if (atoi(subpart) == -1)
            { //if the group is all
              forMe = true;
            }
            else if ( strlen(subpart) > 0 && atoi(subpart) == group)
            { //if the group is all or me
              forMe = true;
            }
            //get next part
            subpart = strtok_r (NULL,  ",>", &subpartPointer);
          }
        }
      }
      else if (currentPart == 3)
      { //one of the command
        command = part;
        if (strcmp(command, "allocate") == 0)
        { //if an allocate command might be for me regardless of id
          forMe = true;
        }
        if (strcmp(command, "group") == 0)
        { //if a group allocation might be for me
          if (REMOTE_DEBUG)
          {
            udp.beginPacket(broadcastIP, localPort);
            char sprintBuffer [50];
            sprintf(sprintBuffer, "%s", "<><><ignore><heard a group command>");
            if (VERBOSE)Serial.println(sprintBuffer);
            udp.write(sprintBuffer);
            udp.endPacket();
          }
          forMe = true;
        }
        if (strcmp(command, "ignore") == 0 || strcmp(command, "server") == 0)
        { //if a debug msg or server allocation
          forMe = false;
        }
      }
      else if (currentPart == 4 )
      { //one of the opperand
        params = part;
      }
      //get next part
      part = strtok_r (NULL,  ",>", &partPointer);
    }
    //now we have all from the msg
    if (forMe)
    {
      executeCommand(command, params, groupToAdd);
    }
  }
  return true;
}
void requestID()
{
  // send a get ID message
  Serial.println("Requesting an ID");
  udp.beginPacket(broadcastIP, localPort);
  ID = random(2, 9);//only use 1 digit rand ids to make char conversion easy
  char sprintBuffer [50];
  sprintf(sprintBuffer, "%s%i%s", "<-1><-1><allocate><-" , ID , ">");
  if (VERBOSE)Serial.println(sprintBuffer);
  udp.write(sprintBuffer);
  ID = ID * -1; //negate the number
  udp.endPacket();
  requestedID = millis();
}
void executeCommand(char* command, char* params, char* groupToAdd)
{ if (VERBOSE)  Serial.println("Execute command: " + String(command));
  if (strcmp(command, "off") == 0)
  { //split the params to find what goes on
    char* part = strtok(params, ",>");
    while (part != NULL)
    {
      if (VERBOSE)Serial.println(part);
      part = part + 4; //skip the led_ tag
      setPinOff(pinNameToNumber(part));
      //get next part
      part = strtok (NULL,  ",>");
    }
  }
  else if (strcmp(command, "on") == 0)
  { //split the params to find what goes on
    char* part = strtok(params, ",>");
    while (part != NULL)
    {
      if (VERBOSE)Serial.println(part);
      part = part + 4; //skip the led_ tag
      setPinOn(pinNameToNumber(part));
      //get next part
      part = strtok (NULL,  ",>");
    }
  }
  else if (strcmp(command, "flash") == 0)
  { //split the params to find what goes on
    char* part = strtok(params, ",>");
    while (part != NULL)
    {
      if (VERBOSE)Serial.println(part);
      part = part + 4; //skip the led_ tag
      setPinFlash(pinNameToNumber(part));
      //get next part
      part = strtok (NULL,  ",>");//potentila bug not using new strok pointer
    }

  }
  else if (strcmp(command, "read") == 0)
  {

  }
  else if (strcmp(command, "override") == 0)
  {//if we want to change the name
char* part = strtok(params, ",>");
    while (part != NULL)
    {
      if (VERBOSE)Serial.println(part);
      ID=atoi(part);//override id
      //get next part
      part = strtok (NULL,  ",>");//potentila bug not using new strok pointer
    }
  }
  else if (strcmp(command, "ping") == 0)
  {
    udp.beginPacket(broadcastIP, localPort);
    char sprintBuffer [50];
    sprintf(sprintBuffer, "%s%i%s", "<><><ignore><" , (ID) , ">");
    if (VERBOSE)Serial.println(sprintBuffer);
    Serial.print("My ID= ");
    Serial.println(ID);
    udp.write(sprintBuffer);
    udp.endPacket();
  }
  else if (strcmp(command, "pingserver") == 0)
  {
    udp.beginPacket(broadcastIP, localPort);
    char sprintBuffer [50];
    sprintf(sprintBuffer, "%s%i%s%i%s", "<",ID,"><><ignore><" , (nameServerID) , ">");
    if (VERBOSE)Serial.println(sprintBuffer);
    Serial.print("My name server ID= ");
    Serial.println(ID);
    udp.write(sprintBuffer);
    udp.endPacket();
  }
  else if (strcmp(command, "group") == 0)
  { //if should add group
    if (REMOTE_DEBUG)
    {
      udp.beginPacket(broadcastIP, localPort);
      char sprintBuffer [50];
      sprintf(sprintBuffer, "%s", "<><><ignore><execute command in group>");
      if (VERBOSE)Serial.println(sprintBuffer);
      udp.write(sprintBuffer);
      udp.endPacket();
    }
    char *groupPointer;
    char* part = strtok_r(params, ",>", &groupPointer);
    while (part != NULL)
    { if (VERBOSE) Serial.println(part);
      if (REMOTE_DEBUG)
      {
        udp.beginPacket(broadcastIP, localPort);
        char sprintBuffer [50];
        sprintf(sprintBuffer, "%s%i%s%s%s", "<><><ignore>< ", ID, " parsing group to ", part, ">");
        if (VERBOSE)Serial.println(sprintBuffer);
        udp.write(sprintBuffer);
        udp.endPacket();
      }
      if (atoi(part) == ID)
      { //if i should be part of the new group
        if (REMOTE_DEBUG)
        {
          udp.beginPacket(broadcastIP, localPort);
          char sprintBuffer [50];
          sprintf(sprintBuffer, "%s", "<><><ignore><matched>");
          if (VERBOSE)Serial.println(sprintBuffer);
          udp.write(sprintBuffer);
          udp.endPacket();
        }
        group = atoi(groupToAdd);
      }
      else
      {
        if (REMOTE_DEBUG)
        {
          udp.beginPacket(broadcastIP, localPort);
          char sprintBuffer [50];
          sprintf(sprintBuffer, "%s", "<><><ignore><no match>");
          if (VERBOSE)Serial.println(sprintBuffer);
          udp.write(sprintBuffer);
          udp.endPacket();
        }
      }

      //get next part
      part = strtok_r (NULL,  ",>", &groupPointer);
    }

  }
  else if (strcmp(command, "allocate") == 0)
  { //keep trACK OF SO i KNOW IF i'M LAST
    if (lastAllocatedID && nameServerAvailable == false) //before checking mine so not acidently reply to self and that noonelse is doing alication
    { //if im highest id pass the batton
      udp.beginPacket(broadcastIP, localPort);
      char sprintBuffer [50];
      int nextID = ID + 1;
      sprintf(sprintBuffer, "%s%s%s%i%s", "<", params, "><-1><allocate><" , (nextID) , ">");
      if (VERBOSE)Serial.println(sprintBuffer);
      udp.write(sprintBuffer);
      udp.endPacket();
      Serial.println("Gave next ID " + String(nextID));
      if (REMOTE_DEBUG)
      {
        udp.beginPacket(broadcastIP, localPort);
        char sprintBuffer [50];
        int nextID = ID + 1;
        sprintf(sprintBuffer, "%s%i%s", "<><><ignore><said next id is ", (nextID), ">");
        if (VERBOSE)Serial.println(sprintBuffer);
        udp.write(sprintBuffer);
        udp.endPacket();
      }
      lastAllocatedID = false;
    }
    if (realIDAllocated == false)
    { //if it's a reply for me look at params and take
      ID = atoi(params); //TODO check proper formatiing
      Serial.println("Got an ID " + String(ID));
      if (REMOTE_DEBUG)
      {
        udp.beginPacket(broadcastIP, localPort);
        char sprintBuffer [50];
        sprintf(sprintBuffer, "%s%i%s", "<><><ignore><set ID as ", (ID), ">");
        if (VERBOSE)Serial.println(sprintBuffer);
        udp.write(sprintBuffer);
        udp.endPacket();
      }
      udp.beginPacket(broadcastIP, localPort);
      char sprintBuffer [50];
      sprintf(sprintBuffer, "%s", "<><><ignore><Connect next Wemos>");
      udp.write(sprintBuffer);
      udp.endPacket();
      realIDAllocated = true;
      if (nameServerAvailable == false)
      { //if noone else doing it take charge of the next guy
        lastAllocatedID = true;
      }
    }
  }
  else
  {
    Serial.println("Unknown command: " + String(command));
  }
}
void updateFlash()
{
  if (millis() > (lastFlashTime + FLASH_PERIOD))
  {
    for (int a = 0; a < sizeof(flashing) / sizeof(flashing[0]); a++)
    {
      if (flashing[a] == 0)
      { //if should flash on
        setPinOn(a);
        flashing[a] = 1;
        if (VERBOSE)Serial.println("turning " + String(a) + " flash on");
      }
      else if (flashing[a] == 1)
      { //if should flash off
        setPinOff(a);
        flashing[a] = 0;
        if (VERBOSE)Serial.println("turning " + String(a) + " flash off");
      }
    }
    lastFlashTime = millis();
  }
}
/**
   turn the passed pin flash
   @param a single pin (numbered )
*/
void setPinFlash(int pin)
{ if (VERBOSE)Serial.println("turning " + String(pin) + " flash");
  pinMode(pin, OUTPUT);
  digitalWrite(pin, HIGH);
  flashing[pin] = 1; //mark flashing and on
}
/**
   turn the passed pin on
   @param a single pin (numbered )
*/
void setPinOn(int pin)
{ if (VERBOSE)Serial.println("turning " + String(pin) + " on");
  pinMode(pin, OUTPUT);
  digitalWrite(pin, HIGH);
  flashing[pin] = -1; //mark not flashing
}
/**
   turn the passed pin off
   @param a single pin (numbered )
*/
void setPinOff(int pin)
{ if (VERBOSE)Serial.println("turning " + String(pin)  + " off");
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  flashing[pin] = -1; //mark not flashing
}
int pinNameToNumber(char* pin)
{
  //pin mapping wrong on esp8266/arduino..../varients/d1/pins_arduino.h for mapping between Dn names and numbers
  int numberedPin = 0;
  if (strcmp(pin, "D0") == 0)
  {
    numberedPin = 16;
  }
  else if (strcmp(pin, "D1") == 0)
  {
    numberedPin = 5;
  }
  else if (strcmp(pin, "D2") == 0)
  {
    numberedPin = 4;
  } else if (strcmp(pin, "D3") == 0)
  {
    numberedPin = 0;
  } else if (strcmp(pin, "D4") == 0)
  {
    numberedPin = 2;
  } else if (strcmp(pin, "D5") == 0)
  {
    numberedPin = 14;
  } else if (strcmp(pin, "D6") == 0)
  {
    numberedPin = 12;
  } else if (strcmp(pin, "D7") == 0)
  {
    numberedPin = 13;
  } else if (strcmp(pin, "D8") == 0)
  { //doesn't seem to work when wifi is on
    numberedPin = 15;
  }
  else
  {
    Serial.println("Invalid pin name:" + String(pin));
  }
  return numberedPin;
}

