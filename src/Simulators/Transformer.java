/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulators;

import java.util.Random;

/**
 *
 * @author Adam
 */
public class Transformer
{

      double scaler = smartgridmodel.Constants.housesInSimulation * 1.1;//houses and winter factor
      private double[] baseLoads =
      {
	    //v1
	    //76000, 82000, 84000, 84000, 86000, 88000, 88000, 90000,
	    //95000, 100000, 95000, 88000, 86000, 80000, 78000,
	    //74500, 78000, 88000, 98000, 118000, 98000, 90000,
	    //78000, 76000, 76000#
	    1655 * scaler, 1554 * scaler, 1178 * scaler, 1171 * scaler,//0-2
	    962 * scaler, 932 * scaler, 874 * scaler, 874 * scaler,//2 4
	    848 * scaler, 869 * scaler, 898 * scaler, 898 * scaler,//4 6
	    936 * scaler, 999 * scaler, 1266 * scaler, 1262 * scaler,//6 8
	    1813 * scaler, 1903 * scaler, 1940 * scaler, 1923 * scaler,//8 10
	    1814 * scaler, 1804 * scaler, 1785 * scaler, 1798 * scaler,//10 12
	    1883 * scaler, 1893 * scaler, 1956 * scaler, 1966 * scaler,//12 14
	    1670 * scaler, 1690 * scaler, 1767 * scaler, 1780 * scaler,//14 16
	    1951 * scaler, 2151 * scaler, 2442 * scaler, 2462 * scaler,//16 18
	    2777 * scaler, 2775 * scaler, 2638 * scaler, 2608 * scaler,//18 20
	    2577 * scaler, 2777 * scaler, 2911 * scaler, 2921 * scaler,//20 22
	    2953 * scaler, 2753 * scaler, 2537 * scaler, 2500 * scaler,//22 24
      };
      private double additiveLoad = 0;
      private double baseLoad;
      private double netLoad;
      private double renewablePercent = 0;
      private double renewablePower;
      private double totalLoad;
      double targetLoad;
      boolean renewableDisabled = false;

      public void addLoad(double toAdd)
      {
	    additiveLoad += toAdd;
      }

      /**
       *
       * @param hour 0-23
       */
      public void setBaseload(int hour, int quater)
      {
	    //System.out.println("wind= " + renewablePower);
	    int index = hour * 2;
	    if (quater >= 2)
	    {
		  index++;
	    }
	    baseLoad = baseLoads[index];
	    //System.out.println("time = " + hour + " " + quater + " is " + index);
	    generateReneable();
	    calcTargetLoad();
      }

      public void calcTargetLoad()
      {
	    targetLoad = Math.max(Constants.target_load, baseLoad);
	    targetLoad += renewablePower;
      }

      public void setRenewable(double renewableIn)
      {

	    if (renewableIn < 0)
	    {
		  renewableDisabled = true;
		  renewablePercent = 0;
	    }
	    else
	    {
		  renewableDisabled = false;
		  renewablePercent = renewableIn;
		  if (renewablePercent > 150)
		  {
			renewablePercent = 150;
		  }
		  else if (renewablePercent < 0)
		  {

			renewablePercent = 0;
		  }
	    }
	    //System.out.println("setRenewable-" + renewableIn + "  %=" + renewablePercent);
      }

      public void generateReneable()
      {
	    if (renewableDisabled == false)
	    {
		  double scale = new Random().nextDouble() - .5;
		  renewablePercent += renewablePercent * scale;
		  if (renewablePercent > 150)
		  {
			renewablePercent = 150;
		  }
		  else if (renewablePercent < 0)
		  {
			scale = new Random().nextInt(5);
			renewablePercent = scale;
		  }
	    }
	    getRenewableLoad();
	    //System.out.println("generateReneable- pow=" + renewablePower + "  %=" + renewablePercent);
      }

      public double getReward()
      {
	    calculateNetLoad();
	    double reward = (-netLoad + targetLoad) * 100 / targetLoad;
	    /*double reward;
	    if (netLoad > targetLoad)
	    {
		  reward = -1000;
	    }
	    else
	    {
		  reward = 1000;
	    }*/
	    // System.out.println("reward= " + reward);
	    return reward;
      }

      public void newQuater()
      {
	    additiveLoad = 0;
      }

      private void calculateNetLoad()
      {
	    netLoad = additiveLoad + baseLoad - (baseLoad * renewablePercent / 100);
	    additiveLoad = 0;//now used
	    getTotalLoad();
	    getRenewableLoad();
      }

      public double getNetLoad()
      {
	    return netLoad;
      }

      public double getTargetLoad()
      {
	    return targetLoad;
      }

      public double getTotalLoad()
      {
	    totalLoad = additiveLoad + baseLoad;
	    return totalLoad;
      }

      public double getBaseLoad()
      {
	    return baseLoad;
      }

      public double getRenewableLoad()
      {
	    renewablePower = (baseLoad * renewablePercent / 100);
	    return renewablePower;
      }

      public double getRenewablePercentage()
      {
	    return renewablePercent;
      }
}
