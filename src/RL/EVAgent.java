/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RL;

/**
 *
 * @author Adam
 */
public class EVAgent extends Agent
{

      public static int ActionToNumber(String name)
      {
	    if (name == "fast")
	    {
		  return 2;
	    }
	    else if (name == "slow")
	    {
		  return 1;
	    }
	    else
	    {
		  return 0;
	    }
      }
      private boolean movedToDone;

      String[] EVActionNames =
      {
	    "fast", "slow", "off"
      };
      String[] EVBatteryStateNames =
      {
	    "0<", "10<", "20<", "30<", "40<", "50<", "60<", "70<", "80<", "90<", "100", "done"
      };
      String[] EVTransformerStateNames =
      {
	    ">20", ">15", ">10", ">5", ">1", ">T", ">-1", ">-5", ">-10", ">-15", "-20>"
      };

      public EVAgent()
      {
	    Policy batteryPolicy = new Policy();
	    batteryPolicy.addToStateSpace(EVBatteryStateNames, EVActionNames);
	    batteryPolicy.name = "Battery";
	    policies.add(batteryPolicy);
	    Policy transformerPolicy = new Policy();
	    transformerPolicy.addToStateSpace(EVTransformerStateNames, EVActionNames);
	    transformerPolicy.name = "TransEV";
	    policies.add(transformerPolicy);
      }

      public void callUpdate(double batteryCharge, double transformerLoad, double targetLoad, double[] reward)
      {
	    if (batteryCharge >= 100 && movedToDone == false)
	    {//moved to done makes charging bad once we are alread at 100
		  movedToDone = true;
		  batteryCharge = 10;
	    }
	    else if (batteryCharge >= 100 && movedToDone == true)
	    {
		  batteryCharge = 11;
		  if (currentAction.getName() != "off")
		  {
			reward[0] = -1000;
		  }
	    }
	    else
	    {
		  movedToDone = false;//not done
		  batteryCharge = (((int) batteryCharge) / 10);
	    }
//do charge policy
	    double nextValueMax = policies.get(0).getMaxActionValue(EVBatteryStateNames[(int) batteryCharge]);
	    if (0 == winningPolicy)
	    {
		  policies.get(0).update(currentAction, reward[0], true, nextValueMax);
		  policies.get(0).currentState = EVBatteryStateNames[(int) batteryCharge];
	    }
	    else
	    {
		  policies.get(0).update(currentAction, reward[0], false, nextValueMax);
		  policies.get(0).currentState = EVBatteryStateNames[(int) batteryCharge];
	    }
//do transofrmer policy
	    //System.out.println("orig= " + transformerLoad);
	    if (transformerLoad > targetLoad * 1.2)
	    {// ">5", 
		  transformerLoad = 0;
	    }
	    else if (transformerLoad > targetLoad * 1.15)
	    {// ">5", 
		  transformerLoad = 1;
	    }
	    else if (transformerLoad > targetLoad * 1.10)
	    {// ">5", 
		  transformerLoad = 2;
	    }
	    else if (transformerLoad > targetLoad * 1.05)
	    {// ">5", 
		  transformerLoad = 3;
	    }
	    else if (transformerLoad > targetLoad * 1.01)
	    {//  ">1",
		  transformerLoad = 4;
	    }
	    else if (transformerLoad > targetLoad)
	    {// ">T", 
		  transformerLoad = 5;
	    }
	    else if (transformerLoad > targetLoad * .99)
	    {//">-1",  
		  transformerLoad = 6;
	    }
	    else if (transformerLoad > targetLoad * .95)
	    {//">-5",
		  transformerLoad = 7;
	    }
	    else if (transformerLoad > targetLoad * .90)
	    {//">-5",
		  transformerLoad = 8;
	    }
	    else if (transformerLoad > targetLoad * .85)
	    {//">-5",
		  transformerLoad = 9;
	    }
	    else
	    {// "-5<"
		  transformerLoad = 10;
	    }

	    //System.out.println("tload= " + transformerLoad + " target= " + targetLoad + " reward= " + reward[1]);
	    nextValueMax = policies.get(1).getMaxActionValue(EVTransformerStateNames[(int) transformerLoad]);
	    if (1 == winningPolicy)
	    {
		  policies.get(1).update(currentAction, reward[1], true, nextValueMax);
		  policies.get(1).currentState = EVTransformerStateNames[(int) transformerLoad];
	    }
	    else
	    {
		  policies.get(1).update(currentAction, reward[1], false, nextValueMax);
		  policies.get(1).currentState = EVTransformerStateNames[(int) transformerLoad];
	    }
      }

      public String getCurrentState()
      {
	    return policies.get(0).currentState + " " + policies.get(1).currentState;
      }

      public void printStateSpaceToConsole()
      {
	    System.out.println("Agent has " + policies.size() + " policies with values:");
	    for (int a = 0; a < policies.size(); a++)
	    {
		  System.out.println(policies.get(a).getString() + "\n");
	    }
      }

}
