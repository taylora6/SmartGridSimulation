/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RL;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Adam
 */
public class State
{

      private String name;
      private double wValue;
      private ArrayList<Action> actions;

      State(String stateName)
      {
	    name = stateName;
	    actions = new ArrayList<>();
	    wValue = 0;
      }

      void addActions(String[] actionNames)
      {
	    for (int a = 0; a < actionNames.length; a++)
	    {
		  actions.add(new Action(actionNames[a]));
	    }
      }

      String getName()
      {
	    return name;
      }

      Action getExplorationAction()
      {
	    return actions.get(new Random().nextInt(actions.size()));
      }

      Action getBestAction()
      {
	    int bestIndex = 0;
	    double bestScore = actions.get(0).getQValue();
	    for (int a = 1; a < actions.size(); a++)
	    {
		  if (bestScore < actions.get(a).getQValue())
		  {
			bestScore = actions.get(a).getQValue();
			bestIndex = a;
		  }
	    }
	    return actions.get(bestIndex);
      }

      Action getAction(String incomingAction)
      {
	    for (int a = 0; a < actions.size(); a++)
	    {
		  if (incomingAction == actions.get(a).getName())
		  {
			return actions.get(a);
		  }
	    }
	    return null;
      }

      void updateWValue(double incommingReward, double actualValue, double bestNext)
      {//WARNNING NOT SURE IF ACTUALL VALUE IS CORECT CANT BE BOTHERED CHECKING
	    wValue = (1 - Constants.alpha) * wValue + Constants.alpha * (actualValue - incommingReward + Constants.gamma * (bestNext));
      }

      double getMaxActionValue()
      {
	    //System.out.println("State- " + this.name);
	    int bestIndex = 0;
	    double bestScore = actions.get(0).getQValue();
	    //System.out.println("score " + bestIndex + " is " + bestScore);
	    for (int a = 1; a < actions.size(); a++)
	    {
		  if (bestScore < actions.get(a).getQValue())
		  {
			bestScore = actions.get(a).getQValue();
			bestIndex = a;
			//System.out.println("score " + bestIndex + " is " + bestScore);
		  }
	    }
	    return bestScore;
      }

      String printActionsToConsole()
      {
	    String output = "";
	    for (int a = 0; a < actions.size(); a++)
	    {
		  output += actions.get(a).getName() + " - " + actions.get(a).getQValue() + "\t";
	    }
	    return output;
      }

      double getWValue()
      {
	    return wValue;
      }

}
