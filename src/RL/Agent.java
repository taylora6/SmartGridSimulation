/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RL;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Adam
 */
public class Agent
{

      protected ArrayList<Policy> policies;
      protected int winningPolicy = 0;
      protected Action currentAction;
      private boolean explore;

      public Agent()
      {
	    explore = true;
	    policies = new ArrayList<>();
      }

      public Action nominateAction()
      {
	    Action toReturn = policies.get(0).selectAction();
	    double toReturnValue = policies.get(0).getCurrentWValue();
	    //System.out.println(policies.get(0).name + " - " + toReturn.getName() + " : " + toReturnValue);
	    int current = 0;
	    for (int a = 1; a < policies.size(); a++)
	    {
		  Action toTest = policies.get(a).selectAction();
		  double toTestValue = policies.get(a).getCurrentWValue();
		  //System.out.println(policies.get(a).name + " - " + " - " + toTest.getName() + " : " + toTestValue);
		  if (explore == false && toTestValue > toReturnValue)
		  {//if exploit dont choose randomly
			toReturn = toTest;
			current = a;
		  }
		  else if (explore == true && new Random().nextInt(policies.size()) == 1)
		  {
			toReturn = toTest;
			current = a;
		  }
	    }

	    currentAction = toReturn;
	    winningPolicy = current;
	    //System.out.println("Winner is " + policies.get(current).name + " - " + " action= " + currentAction.getName());
	    return toReturn;
      }

      public void setExplotation(boolean shouldExploite)
      {
	    explore = !shouldExploite;
	    for (int a = 0; a < policies.size(); a++)
	    {
		  policies.get(a).isExploring = !shouldExploite;
	    }
      }

      public Action getCurrentAction()
      {
	    return currentAction;
      }
}
