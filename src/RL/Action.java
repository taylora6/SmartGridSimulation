/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RL;

/**
 *
 * @author Adam
 */
public class Action
{

      private double qValue;
      private String name;

      Action(String actionName)
      {
	    qValue = 0;
	    name = actionName;
      }

      double getQValue()
      {
	    return qValue;
      }

      void updateQValue(double incommingReward, double nextValue)
      {
	    //System.out.println("old qval= " + qValue);
	    qValue = qValue + Constants.alpha * (incommingReward + Constants.gamma * nextValue) - qValue;

      }

      public String getName()
      {
	    return name;
      }
}
