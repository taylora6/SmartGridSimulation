/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

/**
 *
 * @author Adam
 */
public class Constants
{

      static final int exploringDays = 100;
      static final String address = "192.168.1.255";
      static final long udpBackoff = 200;
      static final int localPort = 2390;
      static String managementImageFullPath = "C:\\Users\\DSG\\Documents\\smartgridmodel\\model_1_manage_story.jpg";//"C:\\Users\\Adam\\Documents\\NetBeansProjects\\smartgridmodel\\model_1_manage.jpg";
      static String confFileFullPath = "C:\\Users\\DSG\\Documents\\smartgridmodel\\conf.txt";//"C:\\Users\\Adam\\Documents\\NetBeansProjects\\smartgridmodel\\conf.txt";
      static final boolean usingModelConfFile=false;
      public static final int housesInSimulation = 16;
    static final int evColour=2;
    static final int heatColour=0;
    static final long  coolDisplayTime=500;
    

      static int commandToInt(Command command)
      {
	    if (command == Command.join)
	    {
		  return 0;
	    }
	    else if (command == Command.allocate)
	    {
		  return 1;
	    }
	    else if (command == Command.off)
	    {
		  return 2;
	    }
	    else if (command == Command.on)
	    {
		  return 3;
	    }
	    else if (command == Command.flash)
	    {
		  return 4;
	    }
	    else if (command == Command.ping)
	    {
		  return 5;
	    }
            else if (command == Command.group)
	    {
		  return 6;
	    }
            else if (command == Command.pingserver)
	    {
		  return 7;
	    }
            else
            {
                return 8;
            }
      }

      public static enum Command
      {
	    join, allocate, off, on, flash, ping, group,pingserver,override
      };
      public static final String[] commandStrings =
      {
	    "join", "allocate", "off", "on", "flash", "ping", "group","pingserver","override"
      };

      static int coloursToInt(Colours colour)
      {
	    if (null != colour)
	    {
		  switch (colour)
		  {
			case red:
			      return 0;
			case green:
			      return 1;
			case blue:
			      return 2;
			case yellow:
			      return 3;
			default:
			      return 4;
		  }
	    }
	    else
	    {
		  return 4;
	    }
      }
      public static final String[] colourStrings =
      {
	    "Red", "Green", "Blue", "Yellow", "White"
      };
      public static final String[] wemosColourStrings =
      {
	    "led_D1", "led_D2", "led_D5", "led_D7", "led_D8"
      };

      public static enum Colours
      {
	    red, green, blue, yellow, white
      };

}
