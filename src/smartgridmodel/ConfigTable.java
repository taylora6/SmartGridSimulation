/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author DSG
 */
public class ConfigTable extends JPanel implements Runnable {

    JButton[] wemosButtons;
    JTextField[] houseTexts;
    JLabel wemosTitle;
    JLabel houseTitle;
    JTextArea messageText;
    JButton submitButton;
    int[] mapping;
    int currentValidMappings = 0;
    boolean mappingReady = false;
    String messageTextText = "";
    private  Color buttonColour;

    ConfigTable() {
        mapping = new int[Constants.housesInSimulation];
        GridBagLayout gridLayout = new GridBagLayout();
        GridBagConstraints cWemos = new GridBagConstraints();
        wemosButtons = new JButton[Constants.housesInSimulation];
        houseTexts = new JTextField[Constants.housesInSimulation];
        wemosTitle = new JLabel("Wemos");
        houseTitle = new JLabel("House Number");
        messageTextText = "No duplicates,\n 1 is first index";
        messageText = new JTextArea(messageTextText);
        submitButton = new JButton("Submit Numbering");
        buttonColour=submitButton.getBackground();
        submitButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                grabMapping();
                ModelWindow.messegeSender.offAll();
            }
        });
        this.setLayout(gridLayout);
        cWemos.gridx = 0;
        cWemos.gridy = 0;
        this.add(wemosTitle, cWemos);
        cWemos.gridx = 1;
        cWemos.gridy = 0;
        this.add(houseTitle, cWemos);
        for (int a = 0; a < Constants.housesInSimulation; a++) {
            wemosButtons[a] = new JButton();
            wemosButtons[a].setMinimumSize(new Dimension(90, 25));
            wemosButtons[a].setPreferredSize(new Dimension(90, 25));
            houseTexts[a] = new JTextField();
            houseTexts[a].setMinimumSize(new Dimension(50, 20));
            houseTexts[a].setPreferredSize(new Dimension(50, 20));
            houseTexts[a].setHorizontalAlignment(JTextField.RIGHT);
            mapping[a] = a;
            wemosButtons[a].setText("Waiting");
            wemosButtons[a].setBackground(Color.red);
            wemosButtons[a].setToolTipText("Light Wemos currently assigned to house " + (a + 1));//lazy way of getting numbers
            wemosButtons[a].addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    String[] split = ((JButton) e.getSource()).getToolTipText().split(" ");
                    int buttonIndex = Integer.parseInt(split[split.length - 1]) - 1;
                    System.out.print("house " + split[split.length - 1] + " is " + mapping[buttonIndex]);
                    ModelWindow.messegeSender.offAll();
                    ModelWindow.messegeSender.sendServerMessage(mapping[buttonIndex], Integer.MIN_VALUE, Constants.Command.on, Constants.wemosColourStrings[4]);
                }
            });
            cWemos.gridx = 0;
            cWemos.gridy = a + 1;
            cWemos.insets = new Insets(0, 10, 0, 0);
            this.add(wemosButtons[a], cWemos);
            houseTexts[a].setText("" + (a + 1));
            cWemos.gridx = 1;
            cWemos.gridy = a + 1;
            this.add(houseTexts[a], cWemos);
        }
        cWemos.gridx = 0;
        cWemos.gridy = Constants.housesInSimulation + 2;
        this.add(submitButton, cWemos);
        cWemos.gridx = 1;
        cWemos.gridy = Constants.housesInSimulation + 2;
        messageText.setMinimumSize(new Dimension(120,100));
        messageText.setPreferredSize(new Dimension(120,100));
        this.add(messageText, cWemos);
        ModelWindow.messegeSender.gatherWemoses();//request any alreay on give me an id
    }

    void grabMapping() {
        mappingReady = true;
        //test mapping is valid       
        int[] isMappedOk = new int[Constants.housesInSimulation];
        for (int a = 0; a < Constants.housesInSimulation; a++) {
            isMappedOk[a] = 0;
        }
        int[] newMapping = new int[mapping.length];
        for (int a = 0; a < Constants.housesInSimulation; a++) {//get the number from each text field and put in new array at correct place
            try{
            int newNumber = Integer.parseInt(houseTexts[a].getText()) - 1;
            newMapping[newNumber] = mapping[a];
            houseTexts[a].setBackground(Color.white);
            if (isMappedOk[newNumber] == 0) {//say is ok
                isMappedOk[newNumber]++;
            } else {//is duplicate
                isMappedOk[newNumber]++;
                mappingReady = false;
            }
            }
            catch(NumberFormatException e)
            {
                houseTexts[a].setText("Fix here");
                houseTexts[a].setBackground(Color.RED);
            }
                    
        }
         for (int a = 0; a < Constants.housesInSimulation; a++) 
         {//now check for any sparseness in mapping
             if(isMappedOk[a]==0)
             {
                 mappingReady = false;
                 break;
             }
                 
         }
        if (mappingReady == true) {
            mapping = newMapping;
            ModelWindow.messegeSender.allocateAll(mapping);
            messageTextText = "Houses set! ";
        } else {
            messageTextText = "Problem in:\n";
            for (int a = 0; a < Constants.housesInSimulation; a++) {
                if (isMappedOk[a] != 1) {
                    if(houseTexts[a].getText().contains("Fix here")==true)
                    {
                        messageTextText+="Row "+(a+1)+" bad number\n";
                    }
                    else
                    {
messageTextText+="Row "+(a+1)+ " duplicate "+houseTexts[a].getText()+"\n";
                    }
houseTexts[a].setBackground(Color.red);
                }
            }
            
        }
        messageTextText=messageTextText.trim();
        messageText.setText(messageTextText);
    }

    @Override
    public void run() {
        
               //         ModelWindow.messegeSender.sendMessage(-1, Integer.MIN_VALUE, Constants.Command.pingserver, "");
        ArrayList<Integer> addresses = ModelWindow.messegeSender.getAddresses();
        for (int a = 0; a < Math.min(addresses.size(),Constants.housesInSimulation); a++) {
            //min used as we may end up with an extra one or two if rapidly reset
            if(addresses.get(a)>200)
            {
            mapping[a] = addresses.get(a);
            wemosButtons[a].setText("Turn On");
            wemosButtons[a].setBackground(buttonColour);
            }
        }        
        
    }

    void clearMapping() {
 mapping = new int[Constants.housesInSimulation];  
    for (int a = 0; a < Constants.housesInSimulation; a++) {

            wemosButtons[a].setText("Waiting");
            wemosButtons[a].setBackground(Color.red);
        }}

}
