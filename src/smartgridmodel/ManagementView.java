/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Adam
 */
public class ManagementView extends JPanel {

    ResizeImagePanel imagePanel;
    JPanel buttonPanel;
    JButton buttonPing;
    JButton buttonAll;
    JButton buttonA;
    JButton buttonB;
    JButton buttonC;
    JButton buttonColour;
    JButton buttonCylon;
    JButton buttonSpiral;
    boolean pingAll = false;
    int toggleA = 0;
    int toggleB = 0;
    int toggleC = 0;
    int colour = -1;
    int cylonCurrent = -1;
    int spiralCurrent = -1;
    int spiralColour = 0;
    long lastCoolDisplayChange = 0;
    ConfigTable wemosPanel;

    public ManagementView(Dimension size) {

        //make the config pannel
        this.setMinimumSize(size);
        this.setPreferredSize(size);
        this.setLayout(new GridBagLayout());
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent evt) {
                imagePanel.fixImageSize();
            }
        }
        );
        GridBagConstraints c = new GridBagConstraints();

        //make stuff
        imagePanel = new ResizeImagePanel();
        buttonPanel = new JPanel(new GridBagLayout());
        buttonPing = new JButton("Ping All");
        buttonPing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wemosPanel.clearMapping();
                pingAll = true;
                ModelWindow.messegeSender.sendMessage(-1, Integer.MIN_VALUE, Constants.Command.ping, "");
                ModelWindow.messegeSender.sendMessage(-1, Integer.MIN_VALUE, Constants.Command.pingserver, "");

            }
        });
        buttonAll = new JButton("Toggle All");
        buttonAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toggleA++;
                toggleA = toggleA % 3;
                toggleB = toggleA;
                toggleC = toggleA;
cylonCurrent=-1;//both off
spiralCurrent=-1;
                sendMessage(-1, toggleA);

            }
        });
        buttonA = new JButton("Toggle A");
        buttonA.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toggleA++;
                toggleA = toggleA % 3;
                sendMessage(1, toggleA);
                cylonCurrent=-1;//both off
spiralCurrent=-1;
            }
        });
        buttonB = new JButton("Toggle B");
        buttonB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toggleB++;
                toggleB = toggleB % 3;
                sendMessage(2, toggleB);
                cylonCurrent=-1;//both off
spiralCurrent=-1;
            }
        });
        buttonC = new JButton("Toggle C");
        buttonC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toggleC++;
                toggleC = toggleC % 3;
                sendMessage(3, toggleC);
                cylonCurrent=-1;//both off
spiralCurrent=-1;
            }
        });
        buttonColour = new JButton("All");
        buttonColour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                colour++;
                if (colour == Constants.colourStrings.length) {
                    colour = -1;
                    buttonColour.setText("All");
                } else {
                    buttonColour.setText(Constants.colourStrings[colour]);
                }
            }
        });
        buttonCylon = new JButton("Cylon");
        buttonCylon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cylonCurrent >= 0) {
                    cylonCurrent = -1;
                } else {
                    cylonCurrent = 0;
                    spiralCurrent=-1;
                }
            }
        });
        buttonSpiral = new JButton("Spiral");
        buttonSpiral.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (spiralCurrent >= 0) {
                    spiralCurrent = -1;
                } else {
                    spiralCurrent = 0;
                    cylonCurrent=-1;
                }
            }
        });
        //make button panet
        GridBagConstraints constraintsButton = new GridBagConstraints();
        constraintsButton.gridx = 0;
        constraintsButton.gridy = 0;
        constraintsButton.insets = new Insets(10, 10, 10, 10);
        constraintsButton.anchor = GridBagConstraints.FIRST_LINE_START;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonPing, constraintsButton);
        constraintsButton.gridx = 1;
        constraintsButton.gridy = 0;
        constraintsButton.anchor = GridBagConstraints.FIRST_LINE_END;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonAll, constraintsButton);
        constraintsButton.gridx = 0;
        constraintsButton.gridy = 1;
        constraintsButton.anchor = GridBagConstraints.LINE_START;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonA, constraintsButton);
        constraintsButton.gridx = 1;
        constraintsButton.gridy = 1;
        constraintsButton.anchor = GridBagConstraints.LINE_END;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonB, constraintsButton);
        constraintsButton.gridx = 0;
        constraintsButton.gridy = 2;
        constraintsButton.anchor = GridBagConstraints.LINE_START;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonC, constraintsButton);
        constraintsButton.gridx = 1;
        constraintsButton.gridy = 2;
        constraintsButton.anchor = GridBagConstraints.LINE_END;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonColour, constraintsButton);
        constraintsButton.gridx = 0;
        constraintsButton.gridy = 3;
        constraintsButton.anchor = GridBagConstraints.LINE_START;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonCylon, constraintsButton);
        constraintsButton.gridx = 1;
        constraintsButton.gridy = 3;
        constraintsButton.anchor = GridBagConstraints.LINE_END;
        constraintsButton.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(buttonSpiral, constraintsButton);
        //add to frame	
        c.anchor = GridBagConstraints.NORTHEAST;
        c.insets = new Insets(0, 0, 0, 0);
        //c.gridwidth = GridBagConstraints.RELATIVE;
        this.add(buttonPanel, c);
        c.anchor = GridBagConstraints.NORTHWEST;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 0, 0, 0);
        // c.gridwidth=GridBagConstraints.
        this.add(imagePanel, c);
//add the wemos pannel and set it to run
        wemosPanel = new ConfigTable();
        c.anchor = GridBagConstraints.SOUTHWEST;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 0, 0, 0);
        this.add(wemosPanel);
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(4);
        executor.scheduleAtFixedRate(wemosPanel, 0, Simulators.Constants.displaySpeed, TimeUnit.MILLISECONDS);
        executor.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                if (System.currentTimeMillis() > (lastCoolDisplayChange + Constants.coolDisplayTime) ){
                    if (cylonCurrent == 0) {//if need to start
                        spiralCurrent = -1;//reset for safty
                        cylonCurrent = 6;
                        ModelWindow.messegeSender.offAll();
                        ModelWindow.messegeSender.sendMessage(cylonCurrent, Integer.MIN_VALUE, Constants.Command.on, Constants.wemosColourStrings[0]);
                    } else if (cylonCurrent > 0) {
                        spiralCurrent = -1;//reset for safty
                        cylonCurrent++;
                        if (cylonCurrent > 20) {//goes 67...13 12...767 using 14-19 for reverse direction
                            cylonCurrent = 7;
                        }
                        ModelWindow.messegeSender.offAll();
                        if (cylonCurrent > 13) {//going backwards
                            int target = 13 - (cylonCurrent - 13);
                            ModelWindow.messegeSender.sendMessage(target, Integer.MIN_VALUE, Constants.Command.on, Constants.wemosColourStrings[0]);
                        } else {
                            ModelWindow.messegeSender.sendMessage(cylonCurrent, Integer.MIN_VALUE, Constants.Command.on, Constants.wemosColourStrings[0]);
                        }
                    } else if (spiralCurrent >= 0) {
spiralCurrent++;
                        ModelWindow.messegeSender.offAll();
                        ModelWindow.messegeSender.sendMessage(spiralCurrent%Constants.housesInSimulation, Integer.MIN_VALUE, Constants.Command.on, Constants.wemosColourStrings[spiralCurrent/Constants.housesInSimulation]);
                    if    (spiralCurrent>Constants.housesInSimulation*Constants.colourStrings.length)
                    {
                        spiralCurrent=0;
                    }
                    }
                                            lastCoolDisplayChange = System.currentTimeMillis();
                }
            }
        }, 0, Simulators.Constants.displaySpeed, TimeUnit.MILLISECONDS);

    }

    /**
     *
     * @param who -1 all 1 a 2 b 3c
     * @param state
     */
    void sendMessage(int who, int state) {
        Constants.Command toDo = Constants.Command.off;
        if (state == 0) {
            toDo = Constants.Command.off;
        } else if (state == 1) {
            toDo = Constants.Command.flash;
        } else {
            toDo = Constants.Command.on;
        }
        if (who == -1) {
            if (colour == -1) {
                for (int a = 0; a < Constants.colourStrings.length; a++) {
                    ModelWindow.messegeSender.sendMessage(-1, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[a]);
                    long currentTimeMillis = System.currentTimeMillis() + Constants.udpBackoff;
                    while (currentTimeMillis > System.currentTimeMillis()) {
                    }//wait		  
                }
            } else {
                ModelWindow.messegeSender.sendMessage(-1, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[colour]);
            }
        } else if (who == 1) {
            if (colour == -1) {
                for (int b = 1; b <= 5; b++) {
                    for (int a = 0; a < Constants.colourStrings.length; a++) {
                        ModelWindow.messegeSender.sendMessage(b, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[a]);
                        long currentTimeMillis = System.currentTimeMillis() + Constants.udpBackoff;
                        while (currentTimeMillis > System.currentTimeMillis()) {
                        }//wait		  
                    }
                }
            } else {
                for (int b = 1; b <= 5; b++) {
                    ModelWindow.messegeSender.sendMessage(b, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[colour]);
                }
            }
        } else if (who == 2) {

            if (colour == -1) {
                for (int b = 6; b <= 13; b++) {
                    for (int a = 0; a < Constants.colourStrings.length; a++) {
                        ModelWindow.messegeSender.sendMessage(b, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[a]);
                        long currentTimeMillis = System.currentTimeMillis() + Constants.udpBackoff;
                        while (currentTimeMillis > System.currentTimeMillis()) {
                        }//wait		  
                    }
                }
            } else {
                for (int b = 6; b <= 13; b++) {
                    ModelWindow.messegeSender.sendMessage(b, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[colour]);
                }
            }
        } else if (who == 3) {
            if (colour == -1) {
                for (int b = 14; b <= 16; b++) {
                    for (int a = 0; a < Constants.colourStrings.length; a++) {
                        ModelWindow.messegeSender.sendMessage(b, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[a]);
                        long currentTimeMillis = System.currentTimeMillis() + Constants.udpBackoff;
                        while (currentTimeMillis > System.currentTimeMillis()) {
                        }//wait		  
                    }
                }
            } else {
                for (int b = 14; b <= 16; b++) {
                    ModelWindow.messegeSender.sendMessage(b, Integer.MIN_VALUE, toDo, Constants.wemosColourStrings[colour]);
                }
            }
        }
    }
}
