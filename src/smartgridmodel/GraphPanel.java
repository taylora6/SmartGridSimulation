/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import java.awt.Color;
import java.sql.Time;
import java.util.ArrayList;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Second;

/**
 *
 * @author Adam
 */
public class GraphPanel extends JPanel implements Runnable
{

      private DynamicTimeSeriesCollection dataset = null;
      private JFreeChart chart = null;
      ArrayList<Float> totalBuffer;
      ArrayList<Float> baseBuffer;
      ArrayList<Float> renewableBuffer;
      ArrayList<Float> targetBuffer;
      final int numberOfDataSets = 4;
      ArrayList<String> lables;
      boolean dataAdded = false;
      int hour = 0;
      int quater = 0;
    private int numberOfDataToShow=96;

      public GraphPanel(String title, int hourIn, int quaterIn)
      {
	    try
	    {
		  hour = hourIn;
		  quater = quaterIn;
		  totalBuffer = new ArrayList<>();
		  baseBuffer = new ArrayList<>();
		  targetBuffer = new ArrayList<>();
		  renewableBuffer = new ArrayList<>();
		  dataset = new DynamicTimeSeriesCollection(numberOfDataSets, numberOfDataToShow, new Second());

		  dataset.setTimeBase(new Second(new Time(hour, quater * 15, 00)));//new Second(0, 0, 0, 23, 1, 2014));
		  dataset.addSeries(new float[1], 0, "Total Load");
		  dataset.addSeries(new float[1], 1, "Base Load");
		  dataset.addSeries(new float[1], 2, "Renewable");
		  dataset.addSeries(new float[1], 3, "Target Load");

		  chart = ChartFactory.createTimeSeriesChart(title, "Time of Day", "Power (Watt)", dataset, true, true, false);
		  final XYPlot plot = chart.getXYPlot();
		  plot.getRenderer().setSeriesPaint(3, Color.black);

		  DateAxis axis = (DateAxis) plot.getDomainAxis();
		  axis.setTickLabelsVisible(false);

		  //axis.setTimeline(SegmentedTimeline.newFifteenMinuteTimeline());
		  //settimeaxis.setFixedAutoRange(10000);
		  // axis.setDateFormatOverride(new SimpleDateFormat("hh:mm:ss"));//hh:mm
		  //SymbolAxis axis = (SymbolAxis)plot.getDomainAxis();
		  // axis.setTickUnit(new DateTickUnit(DateTickUnitType.MINUTE, 15));
		  final ChartPanel chartPanel = new ChartPanel(chart);
		  chart.getPlot().setBackgroundPaint(Color.gray);

		  add(chartPanel);
	    }
	    catch (Exception e)
	    {
		  System.out.println(e.getLocalizedMessage());
		  e.printStackTrace();
	    }
      }

      public void update(float[] value)
      {
	    float[] newData = new float[numberOfDataSets];
	    newData[0] = value[0];
	    newData[1] = value[1];
	    newData[2] = value[2];
	    newData[3] = value[3];
	    dataset.advanceTime();
	    dataset.appendData(newData);
      }

      @Override
      public void run()
      {
	    chart.setTitle(titleTime());
	    if (dataAdded == true || totalBuffer.size() > 0 && baseBuffer.size() > 0 && renewableBuffer.size() > 0)
	    {
		  float[] toDraw =
		  {
			//0, 1, 2, 3
			totalBuffer.remove(0), baseBuffer.remove(0), renewableBuffer.remove(0), targetBuffer.remove(0)
		  };
		  update(toDraw);
		  //System.out.println("plot");
		  dataAdded = false;
	    }
	    else
	    {
		  //System.out.println("nothing to plot");
	    }
      }

      void addloadeData(double[] data)
      {
	    dataAdded = true;
	    totalBuffer.add((float) data[0]);
	    baseBuffer.add((float) data[1]);
	    renewableBuffer.add((float) data[2]);
	    targetBuffer.add((float) data[3]);
      }

      public void setTime(int hourIn, int quaterIn)
      {
	    hour = hourIn;
	    quater = quaterIn;
      }

      private String titleTime()
      {
	    String start = "Current Time - ";
	    String min;
	    String hou;
	    if (quater == 0)
	    {
		  min = ":00";
	    }
	    else
	    {
		  min = ":" + (quater * 15);
	    }
	    if (hour < 10)
	    {
		  hou = "0" + hour;
	    }
	    else
	    {
		  hou = "" + hour;
	    }
	    return start + hou + min;
      }

    void resetSim() {
        totalBuffer.clear();
        renewableBuffer.clear();
        this.baseBuffer.clear();
        this.targetBuffer.clear();
        float[] clearData =
		  {
			//0, 1, 2, 3
			0,0,0,0
		  };
        for(int a=0;a<numberOfDataToShow;a++)
        {
            this.dataset.advanceTime();
        this.dataset.appendData(clearData);
        }
    }

}
