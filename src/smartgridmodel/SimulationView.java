/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import RL.Action;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Adam
 */
public class SimulationView extends JPanel
{

      GraphPanel graphPanel;
      JPanel buttonPanel;
      JButton buttonRun;
      JButton buttonUpWind;
      JButton buttonDownWind;
      JLabel labelCurrentWind;
      JButton buttonReset;
      JButton buttonC;
      JButton buttonColour;
      boolean pingAll = false;
      int currentWind = 0;
      int toggleB = 0;
      int toggleC = 0;
      int colour = -1;
      WorldTable worldTable;
      SimulationWrapper simulator;

      public SimulationView(Dimension size)
      {
	    simulator = new SimulationWrapper();
	    ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(10);
	    executor.scheduleAtFixedRate(simulator, 0, Simulators.Constants.displaySpeed, TimeUnit.MILLISECONDS);
	    worldTable = new WorldTable();
	    // this.setMinimumSize(size);
	    //this.setPreferredSize(size);
	    graphPanel = new GraphPanel("Power Usage", simulator.clock.getHour(), simulator.clock.getQuater());
	    this.setLayout(new GridBagLayout());
	    GridBagConstraints constraintsMain = new GridBagConstraints();
	    GridBagConstraints constraintsButton = new GridBagConstraints();
	    //set the graph to draw
	   // executor.scheduleAtFixedRate(graphPanel, 0, Simulators.Constants.displaySpeed, TimeUnit.MILLISECONDS);

	    buttonPanel = new JPanel(new GridBagLayout());
	    buttonRun = new JButton("Run");
	    buttonRun.addActionListener(new ActionListener()
	    {
		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			if (simulator.isRunning() == 0)
			{//is runnnig so pause
			      simulator.pause();
			      buttonRun.setText("Pause");
			}
			else if (simulator.isRunning() == 1)
			{//is runnnig so pause
			      simulator.pause();
			      buttonRun.setText("Continue");
			}
			else if (simulator.isRunning() == -1)
			{//if no simulation start it
			      runSimulation();
			      buttonRun.setText("Pause");
			}
			else
			{
			      System.out.println("got " + simulator.isRunning());
			}
		  }
	    });
	    buttonUpWind = new JButton("Wind +");
	    buttonUpWind.addActionListener(new ActionListener()
	    {
		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			if (currentWind < 0)
			{
			      currentWind = 10;
			}
			else
			{
			      currentWind += 10;
			      if (currentWind > 150)
			      {
				    currentWind = 150;
			      }
			}
			simulator.setWind(currentWind);
			labelCurrentWind.setText("Current Wind: " + currentWind);
		  }
	    });
	    buttonDownWind = new JButton("Wind -");
	    buttonDownWind.addActionListener(new ActionListener()
	    {

		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			if (currentWind == -1)
			{//override generted wind

			}
			else if (currentWind == 0)
			{//make no wind at all
			      currentWind = -1;
			      labelCurrentWind.setText("Current Wind: Overridden");
			}
			else
			{
			      currentWind -= 10;
			      if (currentWind < 0)
			      {
				    currentWind = 0;
			      }
			      labelCurrentWind.setText("Current Wind: " + currentWind);
			      simulator.setWind(currentWind);
			}

		  }
	    });
	    currentWind = 0;
	    labelCurrentWind = new JLabel("Current Wind: " + currentWind);
	    buttonReset = new JButton("Reset");
	    buttonReset.addActionListener(new ActionListener()
	    {
		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			resetSimulation();
		  }

	    });

	    //make button panet
	    constraintsButton.gridx = 0;
	    constraintsButton.gridy = 0;
	    constraintsButton.insets = new Insets(10, 10, 10, 10);
	    constraintsButton.anchor = GridBagConstraints.FIRST_LINE_START;
	    constraintsButton.fill = GridBagConstraints.HORIZONTAL;
	    buttonPanel.add(buttonRun, constraintsButton);
	    constraintsButton.gridx = 1;
	    constraintsButton.gridy = 0;
	    constraintsButton.anchor = GridBagConstraints.FIRST_LINE_END;
	    constraintsButton.fill = GridBagConstraints.HORIZONTAL;
	    buttonPanel.add(buttonReset, constraintsButton);
	    constraintsButton.gridx = 0;
	    constraintsButton.gridy = 1;
	    constraintsButton.anchor = GridBagConstraints.LINE_START;
	    constraintsButton.fill = GridBagConstraints.HORIZONTAL;
	    buttonPanel.add(buttonUpWind, constraintsButton);
	    constraintsButton.gridx = 1;
	    constraintsButton.gridy = 1;
	    constraintsButton.anchor = GridBagConstraints.LINE_END;
	    constraintsButton.fill = GridBagConstraints.HORIZONTAL;
	    buttonPanel.add(buttonDownWind, constraintsButton);
	    constraintsButton.gridx = 0;
	    constraintsButton.gridy = 2;
	    constraintsButton.gridwidth = 2;
	    constraintsButton.anchor = GridBagConstraints.PAGE_END;
	    constraintsButton.fill = GridBagConstraints.HORIZONTAL;
	    buttonPanel.add(labelCurrentWind, constraintsButton);

	    constraintsMain.anchor = GridBagConstraints.NORTHEAST;
	    constraintsMain.insets = new Insets(10, 10, 10, 10);
	    //c.gridwidth = GridBagConstraints.RELATIVE;
	    this.add(buttonPanel, constraintsMain);
	    constraintsMain.anchor = GridBagConstraints.NORTHWEST;
	    constraintsMain.fill = GridBagConstraints.HORIZONTAL;
	    this.add(graphPanel, constraintsMain);
	    constraintsMain.anchor = GridBagConstraints.NORTHWEST;
	    constraintsMain.fill = GridBagConstraints.HORIZONTAL;
	    this.add(worldTable, constraintsMain);

	    //set up regular adding of data to the graph	
	    //executor.scheduleAtFixedRate(worldTable, 0,Simulators.Constants.displaySpeed, TimeUnit.MILLISECONDS);
	    executor.scheduleAtFixedRate(new Runnable()
	    {
		  @Override
		  public void run()
		  {
			double[] data = simulator.getGraphData();
			if (data != null)
			{
			      graphPanel.addloadeData(data);
                              graphPanel.run();
			}
                        if(simulator.evStatsTaken==0)
                        {
                            ArrayList<Action> evActions = simulator.getEVActions();
			      ArrayList<Action> heatActions = simulator.getHeatingActions();
			      ArrayList<Double> charges = simulator.getCharges();
			      ArrayList<Double> temeratures = simulator.getTemeratures();
			      worldTable.setHeatActions(heatActions);
			      worldTable.setEVActions(evActions);
			      worldTable.setHeatTemperatures(temeratures);
			      worldTable.setEVCharges(charges);
                              worldTable.run();
                        }
			graphPanel.setTime(simulator.clock.getHour(), simulator.clock.getQuater());
			currentWind = (int) simulator.getRenewablePercentage();
			labelCurrentWind.setText("Current Wind: " + currentWind);
		  }
	    }, 0,Simulators.Constants.displaySpeed, TimeUnit.MILLISECONDS);
      }

      private void runSimulation()
      {
          ModelWindow.messegeSender.offAll();//clear previous
	    simulator.requestNewSimulation();
            //set up regular adding of data to the graph	
            

            
      }

      private void resetSimulation()
      {
          ModelWindow.messegeSender.offAll();//clear previous
	    simulator.requestNewSimulation();
                    graphPanel.resetSim();
	    buttonRun.setText("Pause");
      }
}
