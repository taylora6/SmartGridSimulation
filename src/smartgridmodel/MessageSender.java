/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import smartgridmodel.Constants.Command;

/**
 *
 * @author Adam
 */
public class MessageSender
{

      private DatagramSocket socket = null;
      int[] addresses;
      ArrayList<Integer> wemosServerID;

      public MessageSender()
      {
addresses=new int[Constants.housesInSimulation];
	    wemosServerID = new ArrayList<>();
	    setAdressMap();
	    System.err.println("MAKE SURE CORRECT IP SET FOR NETWORK\nCORRECT NETWORK MUST BE SET");
            for(int a=0;a<Constants.wemosColourStrings.length;a++)
            {
                sendMessage(-1,Integer.MIN_VALUE,Constants.Command.off,Constants.wemosColourStrings[a]);
                long currentTimeMillis = System.currentTimeMillis() + Constants.udpBackoff;
				    while (currentTimeMillis > System.currentTimeMillis())
				    {
				    }//wait
            }
            
            new Thread() {
    public void run() {
 
        try {
            int port = Constants.localPort;
            DatagramSocket dsocket = new DatagramSocket(port);
            dsocket.setReceiveBufferSize(1024*10);
            byte[] buffer = new byte[4096];
 
            DatagramPacket packet = new DatagramPacket(buffer,
                buffer.length);
            while (true) {
                dsocket.receive(packet);
                String msg = new String(buffer, 0, packet.getLength());
                //System.out.println(packet.getAddress().getHostName()                        + ": " + msg);
                packet.setLength(buffer.length);
                if((msg.contains("ignore")&&!msg.contains("Connect next Wemos"))||(msg.contains("server")&&!msg.contains("pingserver")))
                {//if is an anounce msg or respond to serverping (not the ping itself)
                String[] split = msg.split(">");
                if(split.length==4)
                {
                  //  System.out.println("adding "+Integer.parseInt(split[3].substring(1)));
                wemosServerID.add(Integer.parseInt(split[3].substring(1)));
                }
                }
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}.start();
      }

      /**
       *
       * @param who
       * @param group minvalue is no group
       * @param command
       * @param payload
       */
      public void sendMessage(int who, int group, Command command, String payload)
      {
	    try
	    {
		  // Convert the arguments first, to ensure that they are valid
		  InetAddress host = InetAddress.getByName(Constants.address);

		  // Construct the socket
		  socket = new DatagramSocket();

		  // Construct the datagram packet
		  String message = makeMessage(who, group, command, payload,false);
		  byte[] data = message.getBytes();
		  DatagramPacket packet = new DatagramPacket(data, data.length, host, Constants.localPort);
		  //System.err.println("Sent: " + message);
		  // Send it
		  socket.send(packet);

	    }
	    catch (Exception e)
	    {
		  System.out.println(e);
	    }
	    finally
	    {
		  if (socket != null)
		  {
			socket.close();
		  }
	    }
      }
/**
       *
       * @param who
       * @param group minvalue is no group
       * @param command
       * @param payload
       */
      public void sendServerMessage(int who, int group, Command command, String payload)
      {
	    try
	    {
		  // Convert the arguments first, to ensure that they are valid
		  InetAddress host = InetAddress.getByName(Constants.address);

		  // Construct the socket
		  socket = new DatagramSocket();

		  // Construct the datagram packet
		  String message = makeMessage(who, group, command, payload,true);
		  byte[] data = message.getBytes();
		  DatagramPacket packet = new DatagramPacket(data, data.length, host, Constants.localPort);
		  //System.err.println("Sent server: " + message);
		  // Send it
		  socket.send(packet);

	    }
	    catch (Exception e)
	    {
		  System.out.println(e);
	    }
	    finally
	    {
		  if (socket != null)
		  {
			socket.close();
		  }
	    }
      }
      private String makeMessage(int who, int group, Command command, String payload,boolean isUsingServerNumber)
      {
	    String message = "<";
	    /* Sample commands:
   General command
  <ID [1..1]><GROUP [0..1]><COMMAND [1..1]><GPIO [1..*]>
  request to join
  <-1><-1><join><invalid_ID>
  ack join
  <invalid_ID><-1><allocate><new_ID>
  turn off arduino 1's led on d4
  <1><><off><led_D4>
  turn on arduino 3's led on d6
  <3><><on><led_D6>
  set arduino 2's leds on D4&D7 flashing
  <2><><flash><led_D4,led_D7>
  put arduino 1,2,3 into group 5 (addressed to all but can be filtered)
  <-1><5><group><1,2,3>";
  Turn all of group 5's leds on D4 on
  <><5><on><led_D4>
  See system
  <-1><><ping><>*/
	    if ( who == -1)
	    {
		  message += who + "><";
	    }
            else if(who > 0)
            {
                if(isUsingServerNumber)
                {
                    message += who + "><";
                }
                else
                {
                message += addresses[who-1] + "><";
                }
            }
	    else
	    {
		  System.err.println("makeMessage - who is wrong: " + who);
	    }
	    if (group > 0 || group == -1)
	    {
		  message += group + "><";
	    }
	    else if (group == Integer.MIN_VALUE)
	    {
		  message += "><";
	    }
	    else
	    {
		  System.err.println("makeMessage - group is wrong: " + group);
	    }
	    message += Constants.commandStrings[Constants.commandToInt(command)] + "><" + payload + ">";
	    return message;
      }

      private void setAdressMap()
      {
	    int mapped = 0;
            if(Constants.usingModelConfFile)
            {
	    try
	    {
		  File file = new File(Constants.confFileFullPath);
		  FileReader fileReader = new FileReader(file);
		  BufferedReader bufferedReader = new BufferedReader(fileReader);
		  String line;
		  while ((line = bufferedReader.readLine()) != null)
		  {
			//System.out.println("Contents of file:" + line);
			String[] split = line.split(",");
			addresses[Integer.parseInt(split[0]) - 1] = Integer.parseInt(split[1]);
			//System.out.println("Too many in conf file slipping rest");
			mapped++;
			if (mapped > Constants.housesInSimulation)
			{
			      System.out.println("Too many in conf file slipping rest");
			      break;
			}
		  }
		  fileReader.close();

	    }
	    catch (IOException e)
	    {
		  e.printStackTrace();
	    }
            }
            else
            {//not using a conf make a 1 to 1 mapping 
                
                for(int a=0;a<Constants.housesInSimulation;a++)
                {
                    addresses[a]=a+1;//so array 0 is hous 1
                }
            }
      }

    void offAll() {
        for (int a = 0; a < Constants.colourStrings.length; a++)
			{
			      sendMessage(-1, Integer.MIN_VALUE, Command.off, Constants.wemosColourStrings[a]);
			      long currentTimeMillis = System.currentTimeMillis() + Constants.udpBackoff;
			      while (currentTimeMillis > System.currentTimeMillis())
			      {
			      }//wait		  
			}  }

    void gatherWemoses() {
        
        try
	    {
		  // Convert the arguments first, to ensure that they are valid
		  InetAddress host = InetAddress.getByName(Constants.address);

		  // Construct the socket
		  socket = new DatagramSocket();

		  // Construct the datagram packet
		  String message = makeMessage(-1,Integer.MIN_VALUE, Constants.Command.pingserver, "",false);
		  byte[] data = message.getBytes();
		  DatagramPacket packet = new DatagramPacket(data, data.length, host, Constants.localPort);
		 // System.err.println("Sent: " + message);
		  // Send it
		  socket.send(packet);

	    }
	    catch (Exception e)
	    {
		  System.out.println(e);
	    }
	    finally
	    {
		  if (socket != null)
		  {
			socket.close();
		  }
	    }

    }

    ArrayList<Integer> getAddresses() {
        Set<Integer> hs=new HashSet<>();
            hs.addAll(wemosServerID);
            wemosServerID.clear();
            wemosServerID.addAll(hs);
        return wemosServerID;  }

    /**
     * override the current allocation
     * @param mapping 
     */
    void allocateAll(int[] mapping) {
        for(int a=0;a<mapping.length;a++)
        {
            //System.out.println("sending "+mapping[a]+" "+(a+1));
            if(mapping[a]>200)
            {//as a valid server id cannot be less than 200
            
        sendServerMessage(mapping[a],Integer.MIN_VALUE,Constants.Command.override,""+(a+1));
            }
        }
                }
}
