/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import RL.Action;
import RL.EVAgent;
import RL.HeatingAgent;
import Simulators.Clock;
import Simulators.EV;
import Simulators.Heating;
import Simulators.Transformer;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Adam
 */
public class SimulationWrapper implements Runnable
{

      ArrayList<EVAgent> evAgents;
      ArrayList<EV> evs;
      ArrayList<HeatingAgent> heatingAgents;
      ArrayList<Heating> heatings;
      Clock clock;
      Transformer transformer;
      int numberOfHouses;
      boolean hasASim = false;
      boolean newSimulationRequested = false;
      boolean dataTakenThisPeriod = false;
      int evStatsTaken = 0;
      ArrayList<Action> evActionsTaken = new ArrayList();
      ArrayList<Action> heatActionsTaken = new ArrayList();

      public SimulationWrapper()
      {
	    evAgents = new ArrayList<>();
	    evs = new ArrayList<>();
	    heatingAgents = new ArrayList<>();
	    heatings = new ArrayList<>();
	    clock = new Clock();
	    clock.pause();
	    clock.setSlowTime(false);//prevent wasted work if runnning no simulation
	    transformer = new Transformer();//prevent null poiinter display
      }

      public void newSimulation(int numberOfHousesIn)
      {
	    hasASim = true;
	    //System.err.println("New simulation");
	    numberOfHouses = numberOfHousesIn;
	    double[] rewards =
	    {
		  0, 0
	    };
	    clock = new Clock();
	    transformer = new Transformer();
	    transformer.setBaseload(clock.getHour(), clock.getQuater());
	    for (int a = 0; a < numberOfHouses; a++)
	    {
		  //make a new one
		  
                  if(a==2||a==1)
                  {
		  evs.add(new EV(true,1));
                  heatings.add(new Heating(0));
                  }
                  else if(a==15)
                  {
		  evs.add(new EV(true,2));
                  heatings.add(new Heating(2));
                  }
                  else if(a==13)
                  {
                      heatings.add(new Heating(1));
                     evs.add(new EV(true,0)); 
                  }
                  else
                  {
                      heatings.add(new Heating(0));
                     evs.add(new EV(true,0)); 
                  }
		  heatingAgents.add(new HeatingAgent());
		  evAgents.add(new EVAgent());
		  //initalise state
		  heatings.get(a).heat(.25, clock.getHour(), 0);
		  heatingAgents.get(a).callUpdate(heatings.get(a).getCurrentTemperature(), heatings.get(a).getTemperatureTarget(),transformer.getNetLoad(), transformer.getTargetLoad(), rewards);
		  evAgents.get(a).callUpdate(evs.get(a).getCharge(), transformer.getNetLoad(), transformer.getTargetLoad(), rewards);//set inital state		  
	    }
	    //learn a bit about the world 

	    for (int a = 0; a < 50000; a++)
	    {
		  //update load
		  transformer.setBaseload(clock.getHour(), clock.getQuater());
		  //see if ev should be away
		  for (int b = 0; b < numberOfHouses; b++)
		  {
			evs.get(b).travel(clock.getHour(), clock.getQuater());
			//System.out.println("House " + b + " battery = " + evAgents.get(b).getCurrentState() + " " + evs.get(b).getCharge() + "% heat = " + heatingAgents.get(b).getCurrentState() + " " + heatings.get(b).getCurrentTemperature() + " c transform = " + transformer.getNetLoad() + " watts");
			//System.out.println(b + "--------" + clock.getHour() + ":" + clock.getQuater() + "--------");
			Action nominatedAction = evAgents.get(b).nominateAction();
			// System.out.println("ev action = " + nominatedAction.getName());
			transformer.addLoad(evs.get(b).charge(.25, EVAgent.ActionToNumber(nominatedAction.getName())));
			nominatedAction = heatingAgents.get(b).nominateAction();
			//System.out.println("heat action = " + nominatedAction.getName());
			transformer.addLoad(heatings.get(b).heat(.25, clock.getHour(), HeatingAgent.ActionToNumber(nominatedAction.getName())));
			rewards[0] = evs.get(b).getReward();
			rewards[1] = transformer.getReward();
			evAgents.get(b).callUpdate(evs.get(b).getCharge(), transformer.getNetLoad(), transformer.getTargetLoad(), rewards);//learn from what happend
			rewards[0] = heatings.get(b).getReward();//reuses reward [1]
			heatingAgents.get(b).callUpdate(heatings.get(b).getCurrentTemperature(), heatings.get(b).getTemperatureTarget(), transformer.getNetLoad(), transformer.getTargetLoad(), rewards);
		  }
		  transformer.newQuater();//clear old load added
		  while (!clock.tick())
		  {
		  }//wait for a tick to be available to move to next 15 min
	    }	    
	    for (int b = 0; b < numberOfHouses; b++)
	    {
		  //now learnt exploit for the display
		  //if (clock.getDaysElaspsed() == Constants.exploringDays && clock.getHour() == 0 && clock.getQuater() == 0)
		  {
			//System.out.println("++++++++++++++Going to exploitation+++++++++++++++++");
			//clock.setSlowTime(true);
			heatingAgents.get(b).setExplotation(true);
			evAgents.get(b).setExplotation(true);
			//evAgents.get(b).printStateSpaceToConsole();
		  }
		  //System.out.println("Agents " + b + "good");
	    }
	    newSimulationRequested = false;
	    //set some stuff so the new sim looks right
	    transformer.setRenewable(4);//so we have some wind to show
	    Random rand = new Random();
	    double heatVar;
	    double chargeVar;
	    for (int b = 0; b < numberOfHouses; b++)
	    {
		  heatVar = Simulators.Constants.target_temperature + (rand.nextDouble() - .5) * 4;
		  heatings.get(b).setCurrentTemperature(heatVar);
		  chargeVar = 60 + rand.nextDouble() * 19;
		  evs.get(b).setCharge(chargeVar);
	    }
            clock.setSlowTime(true);//make sure we go at the display pace
            System.out.print("simulation ready\n");

      }

      @Override
      public void run()
      {
	    if (newSimulationRequested == false)
	    {//if we aren't resetting
		  if (clock.getPaused())
		  {
			//System.err.println("Sim paused");
		  }
		  else if (clock.tick())
		  {
			System.err.println("Sim ticked " + clock.humanReadableTime());
			//wait for a tick to be available to move to next 15 min
			//if here we have ticked
//clear old actions to report
			evActionsTaken.clear();
			heatActionsTaken.clear();
			double[] rewards =
			{
			      0, 0
			};
			//update load
			transformer.setBaseload(clock.getHour(), clock.getQuater());
			transformer.newQuater();//clear old load added //here to minise time not present	
			int actionCount = 0;
			//see if ev should be away
			for (int b = 0; b < numberOfHouses; b++)
			{//loop basically select and execute action
			      evs.get(b).travel(clock.getHour(), clock.getQuater());
			      //System.out.println("House " + b + " battery = " + evAgents.get(b).getCurrentState() + " " + evs.get(b).getCharge() + "% heat = " + heatingAgents.get(b).getCurrentState() + " " + heatings.get(b).getCurrentTemperature() + " c transform = " + transformer.getNetLoad() + " watts");
			      //System.out.println("--------" + clock.getHour() + ":" + clock.getQuater() + "--------");
			      Action nominatedAction = evAgents.get(b).nominateAction();
			      evActionsTaken.add(nominatedAction);
			      //System.out.println("ev action = " + nominatedAction.getName());
			      if (nominatedAction.getName() == "on")
			      {
				    //actionCount++;
			      }
			      double chargeload = evs.get(b).charge(.25, EVAgent.ActionToNumber(nominatedAction.getName()));
			      //System.out.println("chargeload= " + chargeload);
			      transformer.addLoad(chargeload);
			      nominatedAction = heatingAgents.get(b).nominateAction();
			      heatActionsTaken.add(nominatedAction);
			      double heatload = heatings.get(b).heat(.25, clock.getHour(), HeatingAgent.ActionToNumber(nominatedAction.getName()));
			      //System.out.println("chargeload= " + chargeload);
			      transformer.addLoad(heatload);
			      //System.out.println(b + "heatload= " + heatload + " " + nominatedAction.getName());
			      if (nominatedAction.getName() == "on")
			      {
				    actionCount++;
			      }
			      //System.out.println("heat action = " + nominatedAction.getName());
			      //}//dont split//spit loop so agents see the load for reward purposes
			      //System.out.println("Action Count= " + actionCount);
			      //for (int b = 0; b < numberOfHouses; b++)
			      //{//get reward update state			
			      rewards[0] = evs.get(b).getReward();
			      rewards[1] = transformer.getReward();
			      evAgents.get(b).callUpdate(evs.get(b).getCharge(), transformer.getNetLoad(), transformer.getTargetLoad(), rewards);//learn from what happend
			      rewards[0] = heatings.get(b).getReward();
			      heatingAgents.get(b).callUpdate(heatings.get(b).getCurrentTemperature(), heatings.get(b).getTemperatureTarget(), transformer.getNetLoad(), transformer.getTargetLoad(), rewards);
			}
			dataTakenThisPeriod = false;//now we are in a new tick can take data
			evStatsTaken = 0;
		  }
	    }
	    else
	    {
		  //System.err.println("Sim started");
		  this.newSimulation(Constants.housesInSimulation);
	    }
      }

      void pause()
      {
	    clock.pause();
      }

      int isRunning()
      {
	    if (hasASim)
	    {
		  if (clock.getPaused())
		  {
			return 0;
		  }
		  else
		  {
			return 1;
		  }
	    }
	    else
	    {
		  return -1;
	    }

      }

      void requestNewSimulation()
      {
	    this.newSimulationRequested = true;
      }

      /**
       *
       * @return [total, base renewable,target]
       */
      double[] getGraphData()
      {
	    if (dataTakenThisPeriod == false)
	    {
		  double[] output =
		  {
			transformer.getTotalLoad(), transformer.getBaseLoad(), transformer.getRenewableLoad(), transformer.getTargetLoad()
		  };
		  //System.out.println("load= " + output[0] + " " + output[1] + " " + output[2]);
		  dataTakenThisPeriod = true;
		  return output;
	    }
	    return null;
      }

      void setWind(int currentWind)
      {
	    transformer.setRenewable(currentWind);
      }

      ArrayList<Double> getTemeratures()
      {
	    if (evStatsTaken > 3)
	    {
		  return null;
	    }
	    ArrayList<Double> output = new ArrayList();
	    for (int a = 0; a < heatings.size(); a++)
	    {
		  output.add(heatings.get(a).getCurrentTemperature());
	    }
	    evStatsTaken++;
	    return output;
      }

      ArrayList<Double> getCharges()
      {
	    if (evStatsTaken > 3)
	    {
		  return null;
	    }
	    ArrayList<Double> output = new ArrayList();
	    for (int a = 0; a < evs.size(); a++)
	    {
		  output.add(evs.get(a).getCharge());
	    }
	    evStatsTaken++;
	    return output;
      }

      ArrayList<Action> getEVActions()
      {
	    if (evStatsTaken > 3)
	    {
		  return null;
	    }
	    evStatsTaken++;
	    return evActionsTaken;
      }

      ArrayList<Action> getHeatingActions()
      {
	    if (evStatsTaken > 3)
	    {
		  return null;
	    }
	    evStatsTaken++;
	    return heatActionsTaken;
      }

      double getRenewablePercentage()
      {
	    return transformer.getRenewablePercentage();
      }

}
