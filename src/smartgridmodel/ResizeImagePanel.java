/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Adam
 */
public class ResizeImagePanel extends JPanel
{

      JLabel imageHolder;
      BufferedImage image = null;

      public ResizeImagePanel()
      {
	    this.setMinimumSize(new Dimension(500, 500));
	    imageHolder = new JLabel();
	    //imageHolder.setBounds(10, 10, 200, 200);
	    try
	    {
		  image = ImageIO.read(new File(Constants.managementImageFullPath));
	    }
	    catch (IOException e)
	    {
		  e.printStackTrace();
	    }
	    ImageIcon imageIcon = new ImageIcon(fitimage(image, imageHolder.getWidth(), imageHolder.getHeight()));
	    imageHolder.setIcon(imageIcon);
	    /*this.addComponentListener(new ComponentAdapter()
	    {
		  @Override
		  public void componentResized(ComponentEvent evt)
		  {
			
		  }
	    }
	    );*/
	    this.add(imageHolder);
      }

      void fixImageSize()
      {
	    //System.err.println("Bound= " + this.getBounds().x + " " + this.getBounds().y + " " + this.getBounds().width + " " + this.getBounds().height + " ");
	    Rectangle newBounds = new Rectangle(0, 0, this.getBounds().width, this.getBounds().height);
	    imageHolder.setBounds(newBounds);
	    ImageIcon imageIcon = new ImageIcon(fitimage(image, imageHolder.getWidth(), imageHolder.getHeight()));
	    imageHolder.setIcon(imageIcon);
      }

      private Image fitimage(Image img, int w, int h)
      {
	    if (w <= 0)
	    {
		  w = 1;
	    }
	    if (h <= 0)
	    {
		  h = 1;
	    }
	    BufferedImage resizedimage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
	    Graphics2D g2 = resizedimage.createGraphics();
	    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(img, 0, 0, w, h, null);
	    g2.dispose();
	    return resizedimage;
      }
}
