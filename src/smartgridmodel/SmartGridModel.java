/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Adam
 */
public class SmartGridModel
{

      static ModelWindow modelWindow;

      /**
       * @param args the command line arguments
       */
      public static void main(String[] args)
      {
          //first load the configuration
          if(args.length<=0)
          {
            readConf(null);  
          }
          else
          {
          readConf(args[0]);
          }
	    modelWindow = new ModelWindow();
	    //MessageSender messageSender = new MessageSender();
	    //messageSender.sendMessage(10, 2, Constants.Command.join, "LED_2");
	    //System.exit(0);

      }
      
      public static void readConf(String confPath)
      {
          if(confPath==null)
          {
              confPath=System.getProperty("user.dir");
              confPath+="\\smartgridmodel.conf";
              //////System.out.println("made path= "+confPath);
          }
          else
          {
              //System.out.println("got path= "+confPath);
          }
          try
	    {
		  File file = new File(confPath);
		  FileReader fileReader = new FileReader(file);
		  BufferedReader bufferedReader = new BufferedReader(fileReader);
		  String line;
		  while ((line = bufferedReader.readLine()) != null)
		  {
			//System.out.println("Contents of file:" + line);
			String[] split = line.split(">");
			if(split[0].contains("realTimePerTimeStepInMiliseconds"))
                        {//setting the frequency
                            Simulators.Constants.slowProgress=Integer.parseInt(split[1].substring(1));
                        }
                        else if(split[0].contains("managementImagePath"))
                        {//setting the frequency
                            Constants.managementImageFullPath=split[1].substring(1);
                        }
                            
		  }
		  fileReader.close();

	    }
	    catch (IOException e)
	    {
                System.out.println("there was a problem loading the conf file - "+e.getLocalizedMessage());
		  e.printStackTrace();
	    }
      }
}
