\documentclass[]{article}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}
\usepackage{placeins}
\usepackage{tabulary}
\usepackage{booktabs}
\usepackage{color}
\usepackage[capitalize,nameinlink]{cleveref}
%opening
\title{Running the Model}
\author{Adam Taylor}
\newcommand{\mref}[1]{{\color{blue}\cref{#1}}}

\begin{document}

\maketitle


\section{Introduction}
Firstly, some terminology:
\begin{itemize}
\item Platform---Everything needed to do a demo.
\item Model---The physical neighbourhood.
\item Interface-side---The controlling program running on a laptop.
\item Wemos-side---The small units responding to commands from the interface-side.
\item Simulation---A simple demo; time progressing and load being displayed.
\end{itemize}
Running a basic simulation on the model should be fairly simple, the steps are described in \mref{sectBasic}.  Small modifications can be made to for `cosmetic' changes to a simulation, those possible are described in \mref{sectCosmetic}.  Finally, an overview of the whole platform's code and structure is given in \mref{sectModel}.  This information should be helpful, if more serious modification is needed.
\section{Creating a Basic Simulation}\label{sectBasic}
As with any good recipe, we begin with a list of ingredients and quantities (in brackets are optional extras):
\begin{itemize}
\item Model\hfill X1
\item Wemos D1\hfill X16 (1)
\item USB hubs\hfill X3
\item USB cables\hfill X16 (1)
\item LEDs\hfill X16 per colour
\item Jumper wire male to female\hfill X2 per LED
\item Plug board\hfill 5 sockets
\item Laptop\hfill X1
\item Router\hfill X1
\item Code and documentation\hfill X1
\end{itemize}
Once this is all gathered together, the following construction is necessary (if already assembled go to \mref{stepBuiltModel}):
\begin{enumerate}
\item Build the model
\begin{itemize}
\item Connect LEDs to Wemoses.  Currently the colour scheme is LED colour on the anode (positive terminal, longer leg) and one of black, brown, purple, orange or white on the cathode.  The wemos pins are set to D1 red, D2 green, D5 blue, D7 yellow and D8 white.  The on-board LED is linked to D4.
\item Connect the Wemoses to the hubs.  One hub per section (see \mref{figModel}) and house 14 on section C's hub works well.
\item Set the router to broadcast a (mixed) WiFi network with SSID---MildendoNet, password---d0g.d1nr, security---WPA2 personal TKIP.  It must allow and correctly forward broadcasts to 192.168.1.255.
\item Plug the hubs and router into the plug board.
\end{itemize}
\pdfbookmark[1]{Quick Start}{Set-Up Built Model}
\item  \label{stepBuiltModel}Get a complete set of built code and supporting files from \href{https://gitlab.scss.tcd.ie/taylora6/SmartGridSimulation/blob/master/ExternalCopy_20_10_16.zip}{{\color{blue}here [.ZIP]}} using SCSS credentials.  This is a zip of the folder ExternalCopy.  Please keep it up to date as SCSS git does not allow zips of subfolders.  (\href{https://gitlab.scss.tcd.ie/taylora6/SmartGridSimulation/blob/master/Wemos/typeCommands/typeCommands.ino}{{\color{blue}here [.INO]}} provides the optional network listener.)
\item Ensure network is available and the laptop it connected to it, as the interface-side communicates with via broadcasts that will not necessity be forwarded.
\item Optionally connect the spare USB and Wemos to the laptop and run the Arduino program called `typeCommands', turn on the serial monitor and watch the network traffic.
\item Turn on power to the USB hubs turning on all the Wemoses.
\item Wait, after about 1 minute the external blue LEDs will flash once each indicating the hardware test was successful.  After another minute, all Wemoses should be ready.
\item Run the interface-side JAR.  Ensure that the JAR is in a folder with a configuration file called smartgridmodel.conf, a library folder and an image.  Inside the .conf file, change the managementImagePath to the image's path.
\item Change to the Management tab (see \mref{figManagmentView} marked in red), on the right there is a panel which has a series of buttons (see \mref{figManagmentView} marked in yellow).
\item If all of the buttons say `Turn on', then proceed to \mref{stepAllocate}.  Otherwise, press `Ping All' on the left of the interface (see \mref{figManagmentView} marked in purple).  This will ask all of the Wemoses to re-announce themselves, to populate the buttons.  If after about a minute, the are still not populated, start again.
\item \label{stepAllocate}All of the buttons now say `Turn on', press `Submit Numbering' (see \mref{figManagmentView} marked green).  This allocates the Wemoses to houses (sort of randomly).
\item Press `Turn on' for text field with 1 in, the light corresponding to house 1 will come on.  Edit the text field to whichever house came on.  Repeat for all houses.
\item Press `Submit Numbering' to confirm your new allocation.
\item Play with the test buttons on the left until you are satisfied everything works, then switch to the simulation tab.
\item Click `Run' and unsurprisingly, watch the simulation run.
\end{enumerate}
\begin{figure}[hpbt!]
\centering
\includegraphics[width=\textwidth,keepaspectratio]{images/model_1_manage}
\caption{A Suggested Section and Numbering}\label{figModel}
\end{figure}
\begin{figure}[hpbt!]
\centering
\includegraphics[width=\textwidth,keepaspectratio]{images/management_init_anotated}
\caption{The Management View with Some Wemoses Connected}\label{figManagmentView}
\end{figure}
\begin{figure}[hpbt!]
\centering
\includegraphics[width=\textwidth,keepaspectratio]{images/simulation_anotated}
\caption{The Simulation View}\label{figSimulationView}
\end{figure}
\mref{tabSim} shows how each house is configured.
\begin{table}[hbt!]
\centering
    \begin{tabulary}{\textwidth}{lCCCC}    
    \toprule
    \multicolumn{1}{l}{\textbf{House}}&\multicolumn{2}{c}{\textbf{EV}}&\multicolumn{2}{c}{\textbf{Heating}} \\
    \cmidrule(l{1em}r{1em}){2-3}\cmidrule(l{1em}r{1em}){4-5}
     & {Work Hours} & {Charge Power (watt)} & {Temperature} & {Power (watt)} \\
    \midrule
1 & 08:00--16:00 & 3,200  & 22$^\circ$ & 4,000  \\
2 & 08:00--16:00 & 3,200  & 22$^\circ$ & 4,000  \\
3 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
4 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
5 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
6 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
7 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
8 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
9 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
10 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
11 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
12 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
13 & 09:00--17:00 & 3,200/5,600  & 12$^\circ$ & 4,000  \\
14 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
15 & 09:00--17:00 & 3,200/5,600  & 22$^\circ$ & 4,000  \\
16 & 07:00--18:00 & 3,200/5,600  & 22$^\circ$ & 12,000  \\
    \bottomrule
    \end{tabulary}
    \caption{Parameters for Each House}
    \label{tabSim}
\end{table}
\FloatBarrier
%TODO these could go in if people are likely to use them
\section{Changing Settings}\label{sectCosmetic}
Most settings can be fairly easily changed via constants.  The limiting factor will be the interface layout.  It is designed specifically for representing 16 houses.  That said, the simulation will support more, but the interface will need to be repacked.  Particularity the management view.  If more house are being used and the management view is not being changed, then another configuration file can be used.  The (currently disabled) feature reads a file called conf.txt.  The file contains the house number and the ID  (ID not the server ID on wemos) corresponding to it.  These IDs would have to be allocated manually either by connecting power to the Wemoses sequentially or through the `typeCommands' program.
\section{Platform Structure}\label{sectModel}
The platform---when used with the interface---is structured in a star-shaped network.  The Wemoses respond to commands from the interface-side.  The interface-side sends these commands based on user interaction.  A more in-depth description of each side follows.
\subsection{Wemoses}
Each Wemos is an Ardunio programmable interface to the ESP8266 chip.  To program them the default Arduino environment needs some customisation (see \href{https://www.wemos.cc/tutorial/get-started-arduino.html}{{\color{blue}here, but the link tends to move}}).  Once this is done, the `modelCode' can be flashed.  The Wemos's WiFi library is sightly different to the normal Arduino WiFi, so check the documentation. 

Running a sketch does the following.  Connect to the network, generate a serverID, request a houseID, listen for commands.  The serverID functions as a permanent address for the Wemos,  it allows the houseID to be reallocated as needed.  Command are XML based and consist of four fields: \sloppy{$<$ID$><$Group$><$Command$><$Parameters$>$}.  ID is the houseID, serverID or -1 (indicating for all), Group is a shared ID that can be allocated to multiple houses.  Command and Parameters are in \mref{tabCommand}.  Single parameters work better than multiple parameters (something to do with nested strtok), so sending multiple messages to do multiple things is preferable.  It is also worth noting that the pin mapping in the board definition appears to be broken, so a correct mapping function is supplied.
\begin{table}[hbt!]
\centering
    \begin{tabulary}{\textwidth}{llL}    
    \toprule
     \textbf{Command} & \textbf{Parameters}& \textbf{Usage}  \\
    \midrule
    join& invalid houseID& Invalid ID used as temporary identifier when requesting a house\\
    server&serverID&Announce my serverID\\
    allocate&new houseID& The ID to replace the temporary one it is addressed to\\
    override&new houseID&Target specified by serverID must change to new houseID\\
    group&houseID to add&Sent to all with he group to allocate to in Group field\\
    on&Pin to set high&Pins take the form led\textunderscore Dn\\
    off&Pin to set low&Pins take the form led\textunderscore Dn\\
    flash&Pin to alternate&Pins take the form led\textunderscore Dn\\
    ping&none&Request targets respond with houseID\\
    pingserver&none&Request targets respond with serverID\\
    ignore&irrelevant&Ignored by all used for status and debugging\\
    \bottomrule
    \end{tabulary}
    \caption{Commands and Their Uses}
    \label{tabCommand}
\end{table}
\subsection{Interface}
The interface runs as several processes which each update UI elements and the platform's lights.  The management view has two threads, one for populating the connected list and another to update lights based on the test buttons (Cylon and Spiral).  The management view's structure is fairly simple and follows its function.  The only stateful component is the buttons representing the Wemoses and these store their mapping simply in an array indexed by (zero based) house number.

The simulation view is more complex.  There is a thread to run a simulation wrapper and another to take data from that wrapper and update the graph and device state table.  The simulation wrapper provides a way of managing the exploration/exploitation of the scenario.  It has a method which creates simulators for EVs, heating systems and transformer.  It creates agents for the former two types and then trains the agents for a time.  Once training is finished, the wrapper slows down and progresses at an observable rate.  The progress is controlled by a clock, which generates real world time at a variable speed.
\end{document}
