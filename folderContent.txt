folder build - intermediate files
folder dist - location new jars appear, not the preferred location to get code from
folder documentation - source for the documentation
folder ExternalCopy - a complete copy of this folder is sufficient to run the model
folder jfreechart-1.0.19 - the graphing library used
folder nbproject - I used netbeans, provided if you do too
folder src - the source code
folder Wemos - the Wemos code
file conf.txt - a deprecated way to set which house is which
file model\textunderscore1xxxx.jpg - images of the model
file smartgridmodel.conf - an example configuration file